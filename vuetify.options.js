import colors from 'vuetify/es5/util/colors';

export default {
  breakpoint: {},
  icons: {},
  lang: {},
  theme: {
    themes: {
      options: {
        customProperties: true
      },
      light: {
        primary: '#499f94',
        accent: '#38d3c1',
        secondary: '#398da9',
        info: '#5f518a',
        warning: colors.amber.base,
        error: colors.deepOrange.accent4,
        success: colors.green.accent3
      }
    }
  }
}
