[![Netlify Status](https://api.netlify.com/api/v1/badges/2b64f025-b7d0-46b3-8985-7c51c4d337a2/deploy-status)](https://app.netlify.com/sites/heuristic-neumann-2757dd/deploys)

# prismic

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
